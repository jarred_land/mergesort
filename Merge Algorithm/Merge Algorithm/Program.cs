﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Merge_Algorithm
{
    class Program
    {
        public static int[] values = new int[1000];
        public static int[] temp = new int[1000];
        public static int _Mergesort(int[] values, int[] temp, int leftStart, int rightEnd)
        {
            /*problem area
            int middle = (leftStart + rightEnd) / 2;
            if (leftStart > middle)
            {
                /*int middle = (leftStart + rightEnd) / 2;

                return _Mergesort(values, temp, middle + 1, rightEnd);
            }
            else if (rightEnd < middle)
            {

                return _Mergesort(values, temp, middle + 1, rightEnd);
            }
            else 
            {
                return -1;
            }
            MergeHalves(values, temp, leftStart, rightEnd);*/
        }
        public static void MergeHalves(int[] values, int[] temp, int leftStart, int rightEnd)
        {
            int leftEnd = (rightEnd + leftStart) / 2;
            int rightStart = leftEnd + 1;
            int size = rightEnd - leftStart + 1;

            int left = leftStart;
            int right = rightStart;
            int index = leftStart;

            while(left <= leftEnd && right <= rightEnd)
            {
                if (values[left] <= values[right])
                {
                    temp[index] = values[left];
                    index++;
                    left++;
                } else
                {
                    temp[index] = values[right];
                    right++;
                }
                index++;
            }

        }

        static void Main(string[] args)
        {
            int Min = 0;
            int Max = 1000;


            

            Random randNum = new Random();
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = randNum.Next(Min, Max);
            }

            _Mergesort(values, temp, 0, values.Length - 1);
            Console.WriteLine("---------Sorted Array--------");
            foreach (var x in temp)
                Console.Write(x + " ");

            Console.ReadLine();
        }

    }
}
